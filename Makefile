install:
# Install kubectl
ifeq (, $(shell which kubectl))
	$(info installing kubectl)
	curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
	sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
else
	$(info kubectl already installed)
endif

# Install k3d
ifeq (, $(shell which k3d))
	$(info installing k3d)
	wget -q -O - https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | TAG=v5.0.0 bash
else
	$(info k3d already installed)
endif

# Install helm
ifeq (, $(shell which helm))
	$(info installing helm)
	curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
else
	$(info helm already installed)
endif

# Spin up a cluster via k3d
cluster_up:
	k3d cluster create mycluster -s 3 -a 6

# Taint the master nodes so they don't run worker pods
taint_masters:
	for node in $(shell kubectl get nodes --no-headers --selector=node-role.kubernetes.io/master | awk '{print $$1}') ; do \
		kubectl taint node $$node node-role.kubernetes.io/master:NoSchedule ; \
	done

# Tear down the cluster
cluster_down:
	k3d cluster delete mycluster

# Obtain GitLab's helm chart for runners
get_chart:
	helm repo add gitlab https://charts.gitlab.io
	helm repo update gitlab

# Spin up the runners
runners_up:
	kubectl create namespace gitlab-runners || return 0
	helm install --namespace gitlab-runners gitlab-runner -f values.yml gitlab/gitlab-runner

# Stop the runners
runners_down:
	helm delete --namespace gitlab-runners gitlab-runner

# Show status of all pods
status:
	watch -n 1 kubectl get pods -Ao wide